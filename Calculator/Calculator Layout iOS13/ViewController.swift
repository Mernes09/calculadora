//
//  ViewController.swift
//  Calculator Layout iOS13
//
//  Created by Angela Yu on 01/07/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var resultado: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func respuesta(_ sender: UIButton) {
        
        enum OperacionCalculadora {
            case sumar, restar, multiplicar, dividir
            
            init?(_ boton:String) {
                switch boton {
                    case "+": self = .sumar
                    case "-": self = .restar
                    case "*": self = .multiplicar
                    case "/": self = .dividir
                    default: return nil
                }
            }
            
            func calcular(_ n1: Int, _ n2: Int) -> Int {
                switch self {
                    case .sumar: return n1 + n2
                    case .restar: return n1 - n2
                    case .multiplicar: return n1 * n2
                    case .dividir: return n1 / n2
                }
            }
        }
        var n1:Int? = 0, n2:Int? = 0
        var operacion:OperacionCalculadora? = nil
        var pantalla:String = "0"
        func actualizaNumero(_ boton:String) -> Int? {
            n1 = Int(pantalla)
            n2 = Int(boton)
            if n2 != nil {
                pantalla = boton
            }
            return n2
        }
        func actualizaOperacion(_ boton:String) -> OperacionCalculadora? {
            operacion = OperacionCalculadora(boton)
            return operacion
        }
        func realizaCalculo() -> Bool {
            if n2 == 0 && operacion == .dividir {
                print("No se puede dividir por cero\n")
                return false
            }
            pantalla = String(operacion!.calcular(n1!, n2!))
            return true
        }
        repeat {
            print("Primer número: ", terminator:"")
            if actualizaNumero(readLine()!) == nil {
                print("Error\n")
                continue
            }
            print("Operación: ", terminator:"")
            if actualizaOperacion(readLine()!) == nil {
                print("Error\n")
                continue
            }
            print("Segundo número: ", terminator:"")
            if actualizaNumero(readLine()!) == nil {
                print("Error\n")
                continue
            }
            
            if realizaCalculo() == true {
                print("Resultado: " + pantalla + "\n")
            }
        } while true
        
        
        
    }
    
    
}

